CREATE TABLE users (
    id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
    username VARCHAR(50) NOT NULL,
    password  VARCHAR(250),
    PRIMARY KEY (id)
);

CREATE TABLE games (
    id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
    user_id BIGINT NOT NULL,
    count_round_wins INTEGER,
    count_round_losses INTEGER,
    completed BOOLEAN,
    moves_history VARCHAR(50),
    algorithm_id  VARCHAR(40),
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE statistics (
    id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
    user_id BIGINT NOT NULL,
    count_game_wins INTEGER,
    count_game_losses INTEGER,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
