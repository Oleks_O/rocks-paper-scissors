package com.game.rockpaperscissors.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The user has an uncompleted game")
public class GameNotFinishedException extends RuntimeException {

    public GameNotFinishedException(String message) {
        super(message);
    }
}