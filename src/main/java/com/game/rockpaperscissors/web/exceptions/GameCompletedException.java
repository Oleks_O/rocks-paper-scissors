package com.game.rockpaperscissors.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The game is completed")
public class GameCompletedException extends RuntimeException {
}
