package com.game.rockpaperscissors.web;

import com.game.rockpaperscissors.db.entities.StatisticsEntity;
import com.game.rockpaperscissors.db.repositories.StatisticsRepository;
import com.game.rockpaperscissors.models.AuthUser;
import com.game.rockpaperscissors.web.models.Score;
import com.game.rockpaperscissors.web.models.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST-controller for statistics endpoints
 */
@RestController
@RequestMapping("statistics")
public class StatisticsRestController {
    @Autowired
    private StatisticsRepository statisticsRepository;

    /**
     * Get statistics
     * @return statistics
     */
    @GetMapping
    public Statistics getStatistics() {
        StatisticsEntity entity = statisticsRepository.findByUserId(getCurrentUserId());

        Score gamesScore = new Score(entity.getCountGameWins(), entity.getCountGameLosses());

        return new Statistics(gamesScore);
    }

    private long getCurrentUserId() {
        return ((AuthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
    }
}
