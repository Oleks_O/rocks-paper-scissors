package com.game.rockpaperscissors.web;

import com.game.rockpaperscissors.db.entities.GameEntity;
import com.game.rockpaperscissors.models.AuthUser;
import com.game.rockpaperscissors.predictor.Predictor;
import com.game.rockpaperscissors.predictor.algorithms.Algorithm;
import com.game.rockpaperscissors.services.GameService;
import com.game.rockpaperscissors.web.exceptions.GameCompletedException;
import com.game.rockpaperscissors.web.exceptions.GameNotFinishedException;
import com.game.rockpaperscissors.web.models.*;
import com.game.rockpaperscissors.web.exceptions.GameNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.game.rockpaperscissors.web.models.RoundStatus.*;
import static com.game.rockpaperscissors.web.models.RoundStatus.DRAW;

/**
 * REST-controller for game endpoints
 */
@RestController
@RequestMapping("game")
public class GameRestController {
    private static final int MAX_HISTORY_LENGH = 50;

    @Autowired
    private GameService gameService;

    @Autowired
    private Predictor predictor;

    /**
     * Gets a current game
     * @return a current game
     */
    @GetMapping
    public Game getGame() {
        GameEntity entity = fetchGameByUserId(getCurrentUserId());

        return Game.of(entity);
    }

    /**
     * Starts a game
     * @return a game info
     */
    @PutMapping
    public Game createGame() {
        long userId = getCurrentUserId();
        GameEntity gameEntity = gameService.getGameByUserId(userId);
        if (gameEntity != null && !gameEntity.isCompleted()) {
            throw new GameNotFinishedException("A new game cannot be started as the user has an uncompleted game. USER_ID: " + gameEntity.getId());
        }

        GameEntity savedEntity = gameService.createGame(userId);

        return Game.of(savedEntity);
    }

    /**
     * Terminates a current game
     * @return a game state
     */
    @PostMapping("/terminate")
    public Game terminateGame() {
        GameEntity entity = fetchGameByUserId(getCurrentUserId());

        GameEntity terminatedGame = gameService.terminateGame(entity);

        return Game.of(terminatedGame);
    }

    /**
     * Returns a counter move
     * @return a counter move
     */
    @GetMapping("/counterMove")
    public Move counterMove() {
        GameEntity entity = fetchGameByUserId(getCurrentUserId());

        return predictor.nextMove(entity.getAlgorithmId(), entity.getMovesHistory());
    }

    /**
     * Plays a round
     * @param roundMoves - an user move and a counter move
     * @return a round result
     */
    @PostMapping("/playRound")
    public RoundResult playRound(@Valid @RequestBody RoundMoves roundMoves) {
        GameEntity gameEntity = fetchGameByUserId(getCurrentUserId());
        if (gameEntity.isCompleted()) {
            throw new GameCompletedException();
        }

        RoundStatus roundStatus = gameService.calculateRoundStatus(roundMoves);

        String history = appendMovesToHistory(gameEntity.getMovesHistory(), roundMoves);
        gameEntity.setMovesHistory(history);

        if (roundStatus != DRAW) {
            // if a user won or an algorithm does not exist then an algorithm should be changed
            if (!predictor.isAlgorithmEffective(gameEntity.getAlgorithmId(), roundStatus)) {
                Algorithm nextAlgorithm = predictor.getNextAlgorithmAfter(gameEntity.getAlgorithmId());
                gameEntity.setAlgorithmId(nextAlgorithm.getId());
            }

            gameService.saveGameWithRoundStatus(gameEntity, roundStatus);
        } else {
            gameService.saveGame(gameEntity);
        }

        return new RoundResult(roundStatus, Game.of(gameEntity));
    }

    private String appendMovesToHistory(String history, RoundMoves roundMoves) {
        String value = "" + roundMoves.getUserMove().getLetter() + roundMoves.getCounterMove().getLetter();
        String newHistory = history != null ? history + value : value;
        if (newHistory.length() > MAX_HISTORY_LENGH) {
            newHistory = newHistory.substring(newHistory.length() - MAX_HISTORY_LENGH);
        }
        return newHistory;
    }

    private long getCurrentUserId() {
        return ((AuthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
    }

    private GameEntity fetchGameByUserId(long userId) {
        GameEntity gameEntity = gameService.getGameByUserId(userId);
        if (gameEntity == null) {
            throw new GameNotFoundException("Not found a game for the user: " + userId);
        }
        return gameEntity;
    }
}
