package com.game.rockpaperscissors.web.models;

public enum RoundStatus {
    WIN,
    DRAW,
    LOSS
}
