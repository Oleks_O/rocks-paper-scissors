package com.game.rockpaperscissors.web.models;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RoundMoves {
    @NotNull(message = "Please provide a user move")
    private Move userMove;

    @NotNull(message = "Please provide a counter move")
    private Move counterMove;
}
