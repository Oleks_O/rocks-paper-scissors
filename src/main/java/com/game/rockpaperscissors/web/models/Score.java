package com.game.rockpaperscissors.web.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Score {
    private int wins;
    private int losses;

    public Score(int wins, int losses) {
        this.wins = wins;
        this.losses = losses;
    }
}
