package com.game.rockpaperscissors.web.models;

import java.util.Arrays;

public enum Move {
    ROCK('R'),
    PAPER('P'),
    SCISSORS('S');

    private char letter;

    Move(char letter) {
        this.letter = letter;
    }

    public char getLetter() {
        return letter;
    }

    public static Move valueOf(char letter) {
        return Arrays.stream(values()).filter(item -> item.getLetter() == letter).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found Move for the letter: " + letter));
    }

    public static Move winMoveFor(Move move) {
        if (move == ROCK) {
            return PAPER;
        } else if (move == SCISSORS) {
            return ROCK;
        } else {
            return SCISSORS;
        }
    }
}
