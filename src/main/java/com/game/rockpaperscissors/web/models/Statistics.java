package com.game.rockpaperscissors.web.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Statistics {
    private Score gamesScore;

    public Statistics(Score gamesScore) {
        this.gamesScore = gamesScore;
    }
}
