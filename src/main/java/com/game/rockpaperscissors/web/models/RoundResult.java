package com.game.rockpaperscissors.web.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoundResult {
    private RoundStatus status;
    private Game game;

    public RoundResult(RoundStatus status, Game game) {
        this.status = status;
        this.game = game;
    }
}
