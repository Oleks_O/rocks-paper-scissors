package com.game.rockpaperscissors.web.models;

import com.game.rockpaperscissors.db.entities.GameEntity;
import lombok.*;

@Getter
@Setter
public class Game {
    public static Game of(GameEntity entity) {
        Game game = new Game();
        game.setScore(new Score(entity.getCountRoundWins(), entity.getCountRoundLosses()));
        game.setCompleted(entity.isCompleted());
        return game;
    }

    private Score score;

    private boolean completed;
}
