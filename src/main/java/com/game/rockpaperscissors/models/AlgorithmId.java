package com.game.rockpaperscissors.models;

/**
 * IDs of algorithms
 */
public enum AlgorithmId {
    RANDOM, PPM, MARKOV
}
