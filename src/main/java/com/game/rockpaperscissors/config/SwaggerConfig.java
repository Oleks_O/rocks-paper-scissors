package com.game.rockpaperscissors.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.service.ApiInfo.DEFAULT_CONTACT;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.game.rockpaperscissors.web"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Rock Paper Scissors REST API",
                "<b>How to use API:</b><br>" +
                        "1) createGame - create a new Game<br>" +
                        "2) counterMove - gets a counter Move <br>" +
                        "3) playRound - handle an user Move and a counter Move<br>" +
                        "<br>" +
                        "<b>Notice:</b> /game/terminate - terminate a current game and mark the game as lost",
                "API 1.0",
                null,
                DEFAULT_CONTACT,
                null, null, Collections.emptyList());
    }
}

