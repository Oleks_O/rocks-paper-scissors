package com.game.rockpaperscissors.services;

import com.game.rockpaperscissors.db.entities.GameEntity;
import com.game.rockpaperscissors.db.repositories.GameRepository;
import com.game.rockpaperscissors.web.models.Move;
import com.game.rockpaperscissors.web.models.RoundMoves;
import com.game.rockpaperscissors.web.models.RoundStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.game.rockpaperscissors.web.models.RoundStatus.*;

/**
 * The Game service
 */
@Service
public class GameService {
    private static final int MAX_ROUNDS = 10;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private StatisticsService statisticsService;

    /**
     * Create a game
     * @param userId the user id
     * @return a game entity
     */
    @Transactional
    public GameEntity createGame(long userId) {
        int countDeleted = gameRepository.deleteByUserId(userId);
        if (countDeleted == 0) {
            statisticsService.createStatistics(userId);
        }

        GameEntity entity = new GameEntity();
        entity.setUserId(userId);
        return gameRepository.save(entity);
    }

    /**
     * Gets a game entity by the user id
     * @param userId the user id
     * @return a game entity
     */
    public GameEntity getGameByUserId(long userId) {
        return gameRepository.findByUserId(userId);
    }

    /**
     * Deletes game by the user id
     * @param userId the user id
     */
    public void deleteGameByUserId(long userId) {
        gameRepository.deleteByUserId(userId);
    }

    /**
     * Terminates a game
     * @param gameEntity the game entity
     * @return a modified game entity
     */
    @Transactional
    public GameEntity terminateGame(GameEntity gameEntity) {
        statisticsService.incrementGameLosses(gameEntity.getUserId());

        gameEntity.setCountRoundLosses(MAX_ROUNDS);
        gameEntity.setCompleted(true);
        return gameRepository.save(gameEntity);
    }

    /**
     * Save a game according to a round status
     * @param entity game entity
     * @param roundStatus a round status
     */
    @Transactional
    public void saveGameWithRoundStatus(GameEntity entity, RoundStatus roundStatus) {
        if (roundStatus == RoundStatus.WIN) {
            entity.setCountRoundWins(entity.getCountRoundWins() + 1);
            if (entity.getCountRoundWins() == MAX_ROUNDS) {
                entity.setCompleted(true);
                statisticsService.incrementGameWins(entity.getUserId());
            }
        } else {
            entity.setCountRoundLosses(entity.getCountRoundLosses() + 1);
            if (entity.getCountRoundLosses() == MAX_ROUNDS) {
                entity.setCompleted(true);
                statisticsService.incrementGameLosses(entity.getUserId());
            }
        }

        gameRepository.save(entity);
    }

    /**
     * Calculates a round status
     * @param roundMoves - round moves
     * @return a round status
     */
    public RoundStatus calculateRoundStatus(RoundMoves roundMoves) {
        if (roundMoves.getUserMove() == roundMoves.getCounterMove()) {
            return DRAW;
        }

        return Move.winMoveFor(roundMoves.getUserMove()) == roundMoves.getCounterMove() ? LOSS : WIN;
    }

    /**
     * Save a game
     * @param entity a game entity
     */
    public void saveGame(GameEntity entity) {
        gameRepository.save(entity);
    }
}
