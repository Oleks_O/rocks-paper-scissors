package com.game.rockpaperscissors.services;

import com.game.rockpaperscissors.db.entities.StatisticsEntity;
import com.game.rockpaperscissors.db.repositories.StatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class StatisticsService {
    @Autowired
    private StatisticsRepository statisticsRepository;

    StatisticsEntity createStatistics(long userId) {
        StatisticsEntity statisticsEntity = new StatisticsEntity();
        statisticsEntity.setUserId(userId);
        return statisticsRepository.save(statisticsEntity);
    }

    void incrementGameWins(long userId) {
        StatisticsEntity statisticsEntity = statisticsRepository.findByUserId(userId);
        statisticsEntity.setCountGameWins(statisticsEntity.getCountGameWins() + 1);
        statisticsRepository.save(statisticsEntity);
    }

    void incrementGameLosses(long userId) {
        StatisticsEntity statisticsEntity = statisticsRepository.findByUserId(userId);
        statisticsEntity.setCountGameLosses(statisticsEntity.getCountGameLosses() + 1);
        statisticsRepository.save(statisticsEntity);
    }

}
