package com.game.rockpaperscissors.predictor;

/**
 * The exception for Algorithm functionality
 */
class AlgorithmException extends RuntimeException {
    AlgorithmException(String message) {
        super(message);
    }
}

