package com.game.rockpaperscissors.predictor.algorithms;

import com.game.rockpaperscissors.models.AlgorithmId;
import com.game.rockpaperscissors.web.models.Move;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

/**
 * Algorithm: Prediction by Partial Matching (PPM)
 *
 * Example:
 *  - history string is 'RSRSPSSSSRSRS'
 *  - token size is 6 ( = 3 Rounds)
 *
 *  Steps:
 *  - take the last 6 symbols: SSRSRS
 *  - try to find any matches
 *  - as any match is not found so reduce a token size. (6 - ROUND = 4)
 *  - the token is RSRS
 *  - the token has found so find the move after this token. It is P
 *  - return the move PAPER
 */
@Component
public class HistoryAlgorithm implements Algorithm {
    private static final int ROUND_SIZE = 2;
    private static final int TOKEN_SIZE = ROUND_SIZE * 5;

    @Autowired
    private RandomAlgorithm randomAlgorithm;

    @Override
    public AlgorithmId getId() {
        return AlgorithmId.PPM;
    }

    @Override
    public Move nextMove(String movesHistory) {
        if (countRounds(movesHistory) < 2) {
            return getRandomMove();
        }

        Move nextMove = null;
        int tokenSize = Math.min(movesHistory.length() / 2, TOKEN_SIZE);
        if (isOdd(tokenSize)) {
            tokenSize -= 1;
        }
        do {
            int tokenStartIndex = movesHistory.length() - tokenSize - 1;

            String token = movesHistory.substring(tokenStartIndex, tokenStartIndex + tokenSize);

            String beforeTokenPart = movesHistory.substring(0, tokenStartIndex);

            int index = beforeTokenPart.lastIndexOf(token);
            if (index != -1) {
                // if the token is at the end of beforeTokenPart then takes the first symbol of the token
                char nextMoveChar = index + tokenSize < beforeTokenPart.length()
                        ? beforeTokenPart.charAt(index + 1)
                        : token.charAt(0);
                nextMove = Move.valueOf(nextMoveChar);
                break;
            } else {
                tokenSize -= ROUND_SIZE;
            }


        } while (tokenSize > ROUND_SIZE);


        return nextMove != null ? nextMove : getRandomMove();
    }

    private int countRounds(String movesHistory) {
        return StringUtils.isEmpty(movesHistory) ? 0 : movesHistory.length() / ROUND_SIZE;
    }

    Move getRandomMove() {
        return randomAlgorithm.nextMove(null);
    }

    private boolean isOdd(int number) {
        return number % 2 == 1;
    }
}
