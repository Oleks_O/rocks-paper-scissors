package com.game.rockpaperscissors.predictor.algorithms;

import com.game.rockpaperscissors.models.AlgorithmId;
import com.game.rockpaperscissors.web.models.Move;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Algorithm: Random
 *
 * This algorithm is used when others algorithm stopped winning
 */
@Component
public class RandomAlgorithm implements Algorithm {
    private Random rand = new Random();

    @Override
    public AlgorithmId getId() {
        return AlgorithmId.RANDOM;
    }

    @Override
    public Move nextMove(String movesHistory) {
        return Move.values()[rand.nextInt(3)];
    }
}
