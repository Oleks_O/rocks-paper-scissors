package com.game.rockpaperscissors.predictor.algorithms;

import com.game.rockpaperscissors.models.AlgorithmId;
import com.game.rockpaperscissors.web.models.Move;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * Algorithm: Markov Chain
 *
 * Creates a matrix like this:
 *
 * -------------------------------------
 *           | PAPER | ROCK | SCISSORS |
 * -------------------------------------
 *  PAPER    |   1   |  5   |   3      |
 *  ROCK     |   2   |  1   |   1      |
 *  SCISSORS |   0   |  0   |   2      |
 *  ------------------------------------
 *
 *  It finds a cell in the matrix on intersection a previous user move and the next user move and increments the cell value.
 *
 *  For example:
 *   - an user move was PAPER
 *   - the previous user move was ROCK
 *   So a cell value on intersection of the column ROCK and the row PAPER is incremented.
 *
 *
 *  In order to predict a counter move this algorithm takes the last user move and find a row in the matrix for this move.
 *  Then it finds a max value in the row. It allows to understand what the most expected user move.
 *  Afterwards the algorithm returns a move that beats the expected user move.
 */
@Component
public class MarkovAlgorithm implements Algorithm {
    private static final int ROUND_SIZE = 2;

    @Autowired
    private RandomAlgorithm randomAlgorithm;

    @Override
    public AlgorithmId getId() {
        return AlgorithmId.MARKOV;
    }

    @Override
    public Move nextMove(String movesHistory) {
        if (countRounds(movesHistory) < 2) {
            return getRandomMove();
        }

        int[][] matrix = fillMatrixFromHistory(movesHistory);

        // find the last user move
        char lastUserMoveLetter = movesHistory.charAt(movesHistory.length() - 2);
        Move lastUserMove = Move.valueOf(lastUserMoveLetter);

        // find a row for the last user move
        int[] row = matrix[lastUserMove.ordinal()];

        int columnIndexWithMaxValue = findMaxValueIndex(row);

        Move predictedUserMove = Move.values()[columnIndexWithMaxValue];

        // return a move that can beat a predicted user move
        return Move.winMoveFor(predictedUserMove);
    }

    private int[][] fillMatrixFromHistory(String movesHistory) {
        int[][] matrix = new int[3][3];
        for (int[] row : matrix) {
            Arrays.fill(row, 0);
        }

        int index = 0;
        do {
            Move prevUserMove = Move.valueOf(movesHistory.charAt(index));

            index = index + ROUND_SIZE;

            Move nextUserMove = Move.valueOf(movesHistory.charAt(index));

            matrix[prevUserMove.ordinal()][nextUserMove.ordinal()] += 1;
        } while (index < movesHistory.length() - ROUND_SIZE);

        return matrix;
    }

    private int findMaxValueIndex(int[] array) {
        int index = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > array[index]) {
                index = i;
            }
        }
        return index;
    }

    private int countRounds(String movesHistory) {
        return StringUtils.isEmpty(movesHistory) ? 0 : movesHistory.length() / ROUND_SIZE;
    }

    Move getRandomMove() {
        return randomAlgorithm.nextMove(null);
    }
}
