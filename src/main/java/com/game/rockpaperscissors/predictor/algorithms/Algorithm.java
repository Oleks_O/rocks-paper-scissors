package com.game.rockpaperscissors.predictor.algorithms;

import com.game.rockpaperscissors.models.AlgorithmId;
import com.game.rockpaperscissors.web.models.Move;

/**
 * An algorithm to predict a move that may beat a user move
 */
public interface Algorithm {

    /**
     * Gets the algorithm id
     *
     * @return the algorithm id
     */
    AlgorithmId getId();

    /**
     * Predict a move to beat a user move
     *
     * @param movesHistory - previous moves (format 'RSRRSS...')
     * @return a move to beat a user move
     */
    Move nextMove(String movesHistory);

}
