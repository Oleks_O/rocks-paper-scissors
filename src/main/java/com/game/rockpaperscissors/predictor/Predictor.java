package com.game.rockpaperscissors.predictor;

import com.game.rockpaperscissors.models.AlgorithmId;
import com.game.rockpaperscissors.predictor.algorithms.Algorithm;
import com.game.rockpaperscissors.web.models.Move;
import com.game.rockpaperscissors.web.models.RoundStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Random;

import static com.game.rockpaperscissors.web.models.RoundStatus.WIN;

/**
 * Predict a move that can beat a user move
 */
@Component
public class Predictor {
    @Autowired
    private Algorithm[] algorithms;

    private Random rand = new Random();

    /**
     * Return a "win" move
     * @param algorithmId - the algorithm id
     * @param movesHistory - history of moves
     * @return a "win" move
     */
    public Move nextMove(AlgorithmId algorithmId, String movesHistory) {
        Algorithm algorithm = getAlgorithm(algorithmId);

        if (movesHistory == null) {
            movesHistory = "";
        }
        return algorithm.nextMove(movesHistory);
    }

    /**
     * Returns the next algorithm
     * @param currentAlgorithmId - the current algorithm
     * @return the next algorithm
     */
    public Algorithm getNextAlgorithmAfter(AlgorithmId currentAlgorithmId) {
        if (algorithms.length > 1) {
            Algorithm algorithm;
            do {
                algorithm = getNextAlgorithm();
            } while (algorithm.getId() == currentAlgorithmId);
            return algorithm;
        }
        return getAlgorithm(currentAlgorithmId);
    }

    /**
     * Checks if a current algorithm is effective
     * @param currentAlgorithmId current algorithm id
     * @param roundStatus the round status
     * @return true - if a current algorithm is effective
     */
    public boolean isAlgorithmEffective(AlgorithmId currentAlgorithmId, RoundStatus roundStatus) {
        Assert.notEmpty(algorithms, "Not found algorithms");

        if (!existAlgorithm(currentAlgorithmId)) {
            return false;
        }

        // if a user did not win then it is effective one
        if (roundStatus != WIN) {
            return true;
        }

        // just to prevent permanent switching algorithms after a loss
        return false;
    }

    private boolean existAlgorithm(AlgorithmId algorithmId) {
        return Arrays.stream(algorithms).map(Algorithm::getId).anyMatch(id -> id == algorithmId);
    }

    private Algorithm getNextAlgorithm() {
        int newIndex = rand.nextInt(algorithms.length);
        return algorithms[newIndex];
    }

    private Algorithm getAlgorithm(AlgorithmId algorithmId) {
        if (algorithmId == null) {
            return getNextAlgorithm();
        }

        return Arrays.stream(algorithms)
                .filter(item -> item.getId() == algorithmId).findFirst()
                .orElseThrow(
                        () -> new AlgorithmException("Not found algorithm with id: " + algorithmId)
                );
    }
}
