package com.game.rockpaperscissors.db.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "statistics")
@Getter
@Setter
public class StatisticsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "count_game_wins")
    private int countGameWins;

    @Column(name = "count_game_losses")
    private int countGameLosses;
}
