package com.game.rockpaperscissors.db.entities;

import com.game.rockpaperscissors.models.AlgorithmId;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "games")
@Getter
@Setter
public class GameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "count_round_wins")
    private int countRoundWins;

    @Column(name = "count_round_losses")
    private int countRoundLosses;

    private boolean completed;

    @Size(min = 2, max = 50)
    @Column(name = "moves_history")
    private String movesHistory;

    @Column(name = "algorithm_id")
    @Enumerated(EnumType.STRING)
    private AlgorithmId algorithmId;
}
