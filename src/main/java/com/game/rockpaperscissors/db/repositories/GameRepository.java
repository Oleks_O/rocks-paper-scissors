package com.game.rockpaperscissors.db.repositories;

import com.game.rockpaperscissors.db.entities.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<GameEntity, Long> {
    GameEntity findByUserId(long userId);

    int deleteByUserId(long userId);
}
