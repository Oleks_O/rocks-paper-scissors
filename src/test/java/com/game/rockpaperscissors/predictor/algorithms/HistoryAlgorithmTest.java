package com.game.rockpaperscissors.predictor.algorithms;

import com.game.rockpaperscissors.web.models.Move;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.doReturn;

@RunWith(SpringJUnit4ClassRunner.class)
public class HistoryAlgorithmTest {

    @Spy
    private HistoryAlgorithm historyAlgorithm;

    @Before
    public void before() {
        doReturn(Move.ROCK).when(historyAlgorithm).getRandomMove();
    }

    @Test
    public void testLessThanTwoRounds() {
        Move move = historyAlgorithm.nextMove(null);
        Assert.assertEquals(Move.ROCK, move);

        move = historyAlgorithm.nextMove("");
        Assert.assertEquals(Move.ROCK, move);

        move = historyAlgorithm.nextMove("RS");
        Assert.assertEquals(Move.ROCK, move);
    }

    @Test
    public void testCase1() {
        String history = "RSRS";
        Move nextMove = historyAlgorithm.nextMove(history);
        Assert.assertEquals(Move.ROCK, nextMove);
    }

    @Test
    public void testCase2() {
        String history = "PPRR" +
                "RSRS" +
                "SS" +
                "RSRS";
        Move nextMove = historyAlgorithm.nextMove(history);
        Assert.assertEquals(Move.ROCK, nextMove);

    }
}
