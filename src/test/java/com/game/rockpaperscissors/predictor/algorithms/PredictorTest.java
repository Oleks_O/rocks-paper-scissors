package com.game.rockpaperscissors.predictor.algorithms;

import com.game.rockpaperscissors.models.AlgorithmId;
import com.game.rockpaperscissors.predictor.Predictor;
import com.game.rockpaperscissors.services.GameService;
import com.game.rockpaperscissors.web.models.Move;
import com.game.rockpaperscissors.web.models.RoundMoves;
import com.game.rockpaperscissors.web.models.RoundResult;
import com.game.rockpaperscissors.web.models.RoundStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static com.game.rockpaperscissors.web.models.Move.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PredictorTest {
    @Autowired
    private Predictor predictor;

    @Autowired
    private GameService gameService;

    @Test
    public void test() {
        int iterations = 20;
        int countUserLostGames = 0;

        for (int i = 0; i < iterations; i++) {
            List<Move> userMoves = Arrays.asList(ROCK, PAPER, ROCK, PAPER, SCISSORS, ROCK,
                    SCISSORS, PAPER, SCISSORS, PAPER, ROCK, PAPER, ROCK, PAPER, ROCK, PAPER, ROCK, PAPER, ROCK, PAPER, ROCK, PAPER);

            AlgorithmId algorithmId = predictor.getNextAlgorithmAfter(null).getId();
            int countUserWins = 0;
            int countUserLosses = 0;
            String history = "";

            for (Move userMove : userMoves) {
                Move counterMove = predictor.nextMove(algorithmId, history);

                RoundMoves roundMoves = new RoundMoves();
                roundMoves.setUserMove(userMove);
                roundMoves.setCounterMove(counterMove);
                RoundStatus roundStatus = gameService.calculateRoundStatus(roundMoves);
                if (!predictor.isAlgorithmEffective(algorithmId, roundStatus)) {
                    algorithmId = predictor.getNextAlgorithmAfter(algorithmId).getId();
                }
                if (roundStatus == RoundStatus.WIN) {
                    countUserWins += 1;
                }
                if (roundStatus == RoundStatus.LOSS) {
                    countUserLosses += 1;
                }
                history += "" + userMove.getLetter() + counterMove.getLetter();
            }
            if (countUserWins < countUserLosses) {
                countUserLostGames += 1;
            }
        }

        double percentOfLostGame = ((double) countUserLostGames) / iterations;

        // A user must loss at least in 60% cases on this test data
        Assert.assertTrue(percentOfLostGame  > 0.6);
    }


}
