package com.game.rockpaperscissors.predictor.algorithms;

import com.game.rockpaperscissors.web.models.Move;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.doReturn;

@RunWith(SpringJUnit4ClassRunner.class)
public class MarkovAlgorithmTest {

    @Spy
    private MarkovAlgorithm markovAlgorithm;

    @Before
    public void before() {
        doReturn(Move.ROCK).when(markovAlgorithm).getRandomMove();
    }

    @Test
    public void testLessThanTwoRounds() {
        Move move = markovAlgorithm.nextMove(null);
        Assert.assertEquals(Move.ROCK, move);

        move = markovAlgorithm.nextMove("");
        Assert.assertEquals(Move.ROCK, move);

        move = markovAlgorithm.nextMove("RS");
        Assert.assertEquals(Move.ROCK, move);
    }

    @Test
    public void testCase1() {
        String history = "RS" +
                "PR" +
                "RP";
        Move nextMove = markovAlgorithm.nextMove(history);
        Assert.assertEquals(Move.SCISSORS, nextMove);
    }

    @Test
    public void testCase2() {
        String history = "RP" +
                "SP" +
                "RS" +
                "PP" +
                "SS";
        Move nextMove = markovAlgorithm.nextMove(history);
        Assert.assertEquals(Move.PAPER, nextMove);

        history += "RR";
        nextMove = markovAlgorithm.nextMove(history);
        Assert.assertEquals(Move.SCISSORS, nextMove);
    }
}
