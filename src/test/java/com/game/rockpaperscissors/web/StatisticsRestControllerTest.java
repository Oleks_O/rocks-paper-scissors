package com.game.rockpaperscissors.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.rockpaperscissors.config.SecurityConfig;
import com.game.rockpaperscissors.db.entities.GameEntity;
import com.game.rockpaperscissors.services.GameService;
import com.game.rockpaperscissors.web.models.Move;
import com.game.rockpaperscissors.web.models.RoundMoves;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Arrays.stream;
import static org.hamcrest.Matchers.isIn;
import static org.mockito.Mockito.doReturn;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Import(SecurityConfig.class)
@AutoConfigureTestDatabase
@Transactional
public class StatisticsRestControllerTest {
    private static final long USER_ID = 1L;
    private static final String USER = "user1";
    private static final String PASSWORD = "123456";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @SpyBean
    private GameService gameService;

    @Test
    public void testStatisticsForJustCreateGame() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        mvc.perform(get("/statistics")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json("{'gamesScore':{'wins':0,'losses':0}}"));
    }

    @Test
    public void testStatisticsAfterLastRound() throws Exception {
        // create a game
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        // play and win the last round
        GameEntity gameEntity = new GameEntity();
        gameEntity.setUserId(USER_ID);
        gameEntity.setCountRoundWins(9);
        doReturn(gameEntity).when(gameService).getGameByUserId(USER_ID);

        RoundMoves roundMoves = new RoundMoves();
        roundMoves.setUserMove(Move.PAPER);
        roundMoves.setCounterMove(Move.ROCK);
        mvc.perform(post("/game/playRound")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roundMoves))
                .with(httpBasic(USER, PASSWORD)));

        // create one more game
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        // play and loose the last round
        gameEntity = new GameEntity();
        gameEntity.setUserId(USER_ID);
        gameEntity.setCountRoundLosses(9);
        doReturn(gameEntity).when(gameService).getGameByUserId(USER_ID);

        roundMoves = new RoundMoves();
        roundMoves.setUserMove(Move.ROCK);
        roundMoves.setCounterMove(Move.PAPER);
        mvc.perform(post("/game/playRound")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roundMoves))
                .with(httpBasic(USER, PASSWORD)));

        // check statistics
        mvc.perform(get("/statistics")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json("{'gamesScore':{'wins':1,'losses':1}}"));
    }
}
