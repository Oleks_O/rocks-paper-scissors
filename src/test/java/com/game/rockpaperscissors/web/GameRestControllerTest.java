package com.game.rockpaperscissors.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.rockpaperscissors.config.SecurityConfig;
import com.game.rockpaperscissors.db.entities.GameEntity;
import com.game.rockpaperscissors.services.GameService;
import com.game.rockpaperscissors.web.models.Move;
import com.game.rockpaperscissors.web.models.RoundMoves;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.isIn;
import static org.mockito.Mockito.doReturn;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Import(SecurityConfig.class)
@Transactional
@AutoConfigureTestDatabase
public class GameRestControllerTest {
    private static final String USER = "user1";
    private static final String PASSWORD = "123456";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @SpyBean
    private GameService gameService;


    @Test
    public void whenGameNotStarted_thenReturn400() throws Exception {
        mvc.perform(get("/game")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("Not found game for the user")));
    }

    @Test
    public void testCreateGame() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json("{'score':{'wins':0,'losses':0},'completed':false}", true));
    }

    @Test
    public void whenCreateGameWithUncompletedGame_thenReturn400() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("The user has an uncompleted game")));
    }

    @Test
    public void testCreateGameAfterTerminatedGame() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        mvc.perform(post("/game/terminate")
                .with(httpBasic(USER, PASSWORD)));

        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json("{'score':{'wins':0,'losses':0},'completed':false}", true));
    }

    @Test
    public void testTerminateGame() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        mvc.perform(post("/game/terminate")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json("{'score':{'wins':0,'losses':10},'completed':true}", true));
    }

    @Test
    public void testNextMove() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        mvc.perform(get("/game/counterMove")
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().string(
                        isIn(stream(Move.values()).map(item -> "\"" + item.name() + "\"").collect(toList()))
                ));
    }

    @Test
    public void testPlayRound() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        RoundMoves roundMoves = new RoundMoves();
        roundMoves.setUserMove(Move.PAPER);
        roundMoves.setCounterMove(Move.ROCK);

        mvc.perform(post("/game/playRound")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roundMoves))
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json(  "{'status':'WIN','game':{'score':{'wins':1,'losses':0}}}"));

        roundMoves = new RoundMoves();
        roundMoves.setUserMove(Move.ROCK);
        roundMoves.setCounterMove(Move.PAPER);
        mvc.perform(post("/game/playRound")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roundMoves))
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json(  "{'status':'LOSS','game':{'score':{'wins':1,'losses':1}}}"));

        roundMoves = new RoundMoves();
        roundMoves.setUserMove(Move.ROCK);
        roundMoves.setCounterMove(Move.ROCK);
        mvc.perform(post("/game/playRound")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roundMoves))
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json(  "{'status':'DRAW','game':{'score':{'wins':1,'losses':1}}}"));

    }

    @Test
    public void testPlayLastRound() throws Exception {
        mvc.perform(put("/game")
                .with(httpBasic(USER, PASSWORD)));

        GameEntity gameEntity = new GameEntity();
        gameEntity.setUserId(1L);
        gameEntity.setCountRoundWins(9);
        doReturn(gameEntity).when(gameService).getGameByUserId(1);

        RoundMoves roundMoves = new RoundMoves();
        roundMoves.setUserMove(Move.PAPER);
        roundMoves.setCounterMove(Move.ROCK);
        mvc.perform(post("/game/playRound")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roundMoves))
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().json(  "{'status':'WIN','game':{'score':{'wins':10,'losses':0},'completed':true}}"));
    }

    @Test
    public void playRoundForCompletedGame_thenReturn400() throws Exception {
        GameEntity gameEntity = new GameEntity();
        gameEntity.setCompleted(true);
        doReturn(gameEntity).when(gameService).getGameByUserId(1);

        RoundMoves roundMoves = new RoundMoves();
        roundMoves.setUserMove(Move.ROCK);
        roundMoves.setCounterMove(Move.ROCK);
        mvc.perform(post("/game/playRound")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roundMoves))
                .with(httpBasic(USER, PASSWORD)))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("The game is completed")));

    }
}
