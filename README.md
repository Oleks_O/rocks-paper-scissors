# rocks-paper-scissors

The API for the game 'Rock Paper Scissors'

## Documentation

The API Documentation is located at http://"apihost":8080/api/swagger-ui.html (where apihost - is a host with API)

Steps to start a game:
1) Starts a game (PUT api/game)
2) Gets a counter move (GET api/game/counterMove)
3) Play round with an user move and a counter move (POST api/game/playRound)

api/terminate - terminates a game. (This game is consider for a user as lost)
api/statistics - return statistics for a user

User registration is not implemented on this stage.
So please use the test users to authorize:
user1/123456
user2/123456

##Usage

```bash
mvnw spring-boot:run
```

## Embedded DB

http://"apihost":8080/api/h2-console (where apihost - is a host with API)

Information about connection settings can be found in main/java/resources/application.properties
